import matplotlib.pyplot as plt

from sklearn import datasets
from sklearn import svm

digits = datasets.load_digits()

##Print information about the data
##print(digits.data)
##print(digits.target)
##print(digits.target[3])

##Print the length of the array
##print(len(digits.data))

clf = svm.SVC(gamma=0.001, C=100)
x,y = digits.data[:-5], digits.target[:-5]
clf.fit(x,y)

print('Prediction: ', clf.predict(digits.data[-4]))
plt.imshow(digits.images[-4], cmap=plt.cm.gray_r, interpolation="nearest")
plt.show()
